import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProvidersResponse} from '../../models/providers/ProvidersResponse';
import {Observable, of} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import { HTTP_REQUEST_MESSAGE, UNKNOWN } from '../../constants/cosntants';
import {Provider} from '../../models/providers/Provider';
import {ProviderFilterById} from "../../models/providers/ProviderFilterById";

@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  private providerURL = environment.PROVIDER_API;
  constructor(
    private http: HttpClient
  ) { }

  getProviders(): Observable<ProvidersResponse> {
    return this.http
      .get<ProvidersResponse>(`${this.providerURL}/`)
      .pipe(catchError(this.providerServicesHandleError<ProvidersResponse>()));
  }

  createProvider(params: Provider): Observable<ProvidersResponse> {
    return this.http
      .post<ProvidersResponse>(`${this.providerURL}/`, params)
      .pipe(catchError(this.providerServicesHandleError<ProvidersResponse>()));
  }

  updateProvider(params: Provider): Observable<ProvidersResponse> {
    return this.http
      .post<ProvidersResponse>(`${this.providerURL}/update`, params)
      .pipe(catchError(this.providerServicesHandleError<ProvidersResponse>()));
  }

  getProvider(params: ProviderFilterById): Observable<ProvidersResponse> {
    return this.http
      .post<ProvidersResponse>(`${this.providerURL}/detail`, params)
      .pipe(catchError(this.providerServicesHandleError<ProvidersResponse>()));
  }

  private providerServicesHandleError<T>() {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      const errorCode = (error.error && error.error.message.code) ? error.error.message.code : UNKNOWN;
      // TODO: better job of transforming error for user consumption
      const messaggeError: any = `${HTTP_REQUEST_MESSAGE} ${errorCode}`;
      // Let the app keep running by returning an empty result.
      return of(messaggeError as T);
    };
  }
}
