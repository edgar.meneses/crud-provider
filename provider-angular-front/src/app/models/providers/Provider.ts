export interface Provider {
  id?: number;
  name: string;
  createAt?: Date;
  phone: string;
  address: string;
}
