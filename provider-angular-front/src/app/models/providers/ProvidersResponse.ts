import {Provider} from './Provider';

export interface ProvidersResponse {
  message: string;
  data: Provider[];
}
