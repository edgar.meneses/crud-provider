import { Component, OnInit } from '@angular/core';
import {ProviderService} from '../../services/provider/provider.service';
import {ProvidersResponse} from '../../models/providers/ProvidersResponse';
import {Provider} from '../../models/providers/Provider';

@Component({
  selector: 'app-provider',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.css']
})
export class ProviderComponent implements OnInit {

  providers: Provider[];
  name = '';
  address = '';
  phone = '';
  code = '';

  constructor(
    private providerService: ProviderService
  ) {
  }

  async ngOnInit() {
    this.getProviders();
  }

  async getProviders() {
    this.providerService.getProviders()
      .subscribe(response => {
        this.providers = response.data;
      });
  }

  async createProvider() {
    const provider = {
      name: this.name,
      address: this.address,
      phone: this.phone,
    };

    this.providerService.createProvider(provider)
      .subscribe((response) => {
        this.getProviders();
      });
  }
}
