# CRUD Provider

Este repositorio es un monorepo que expone servicos para poder Crear, Actualizar y Consultar provedores desde una base de datos postgresql y mediante un componente front. 

## Provider-ms

Provider-ms es un microservicio desarrollado en java 8, con sprint boot y gradle para exponser las funciones de lectura, creacion y actualizacion de proveedores. 

## Provider-angular-front

Provider-angular-front es una applicacion desarrollada en Angular 8 que expone una interface de usuario (UI) par consumir los servicios expuestos en el componente provider-ms

## Instalacion 

Para poder ejecutar este proyecto es necesario que cuente con el JDK de Java en su version 8 asi como las instalacion de Node, npm y angular cli para mayor información consule:
>
> [Node](https://nodejs.org/es/download/)
>
> [Angular](https://angular.io/)
>
> [Angular CLI](https://cli.angular.io/)
>
> [JDK](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)

## Uso

Consuma los servicios expuestos por el puerto 9091 y con el base path ceiba-lab/providers/api/v1 
Los servicios expuestos son: 
>    1. Consultar Proveedores:
>        method: GET
>        path: /
>
>    2. Crear Proveedor
>        method: POST
>        path: /
>        body: {"name":"", "address":"", "phone":""}
>
>    3. Consultar Proveedor Por Codigo
>        method: POST
>        path: /detail
>        body: {"code":""}
>
>    4. Actualizar Proveedor
>        method: POST
>        path: /update
>        body:  {"name":"", "address":"", "phone":""}

Nota: Estos servicios se especifican con mayor detalle en el documento swagger


## Contribucion

Todos los pull request son bienvenidos. Para cambios importantes, porfavor abra un issue primero para discutir que le gustria cambiar. 
Porfavor asegurese de actualizar las pruebas segun el cambio realizado.

## License
[MIT](https://choosealicense.com/licenses/mit/)


