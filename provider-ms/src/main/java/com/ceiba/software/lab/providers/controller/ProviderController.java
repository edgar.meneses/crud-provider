package com.ceiba.software.lab.providers.controller;

import java.util.List ;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ceiba.software.lab.providers.exceptions.BeanBadRequestException;
import com.ceiba.software.lab.providers.exceptions.BeanNotAutorizedException;
import com.ceiba.software.lab.providers.model.Provider;
import com.ceiba.software.lab.providers.model.request.ProviderFilterById;
import com.ceiba.software.lab.providers.model.request.ProviderFilterUpdate;
import com.ceiba.software.lab.providers.model.response.Response;
import com.ceiba.software.lab.providers.service.ProviderService;
import static com.ceiba.software.lab.providers.utils.ResponseHandler.*;
import static com.ceiba.software.lab.providers.utils.GetDayFromDate.*;
@RestController
@RequestMapping("providers/api/v1")
public class ProviderController {

	@Autowired
	private ProviderService serivices;

	@CrossOrigin
	@GetMapping("/")
	public ResponseEntity<Response> getProviders() {
		List<Provider> providerList = serivices.getProviders();
		return successResponse(providerList, "success"); 
	}

	@CrossOrigin
	@PostMapping("/details")
	public ResponseEntity<Response> getProvider(
			@RequestBody ProviderFilterById body,
			Errors errors) {
		if(errors.hasErrors()) {
			throw new BeanBadRequestException();
		}else {
			long id = body.getCode();
			Provider provider = serivices.getProvider(id);
			return successResponse(provider, "the provider with code "+ "code was to find");
		}

	}

	@CrossOrigin
	@PostMapping("/")
	public ResponseEntity<Response> createProvider(
			@RequestBody @Valid Provider body,
			Errors errors) {
		if(errors.hasErrors()) {
			throw new BeanBadRequestException();
		}else if( isSunnday()) {
			throw new BeanNotAutorizedException();
		}
		else {
			Provider newProvider = serivices.createProvider(body);
			return successResponse(newProvider, "the provider was create success");
		}
	}

	@CrossOrigin
	@PostMapping("/update")
	public ResponseEntity<Response> updateProvider(
			@Valid @RequestBody ProviderFilterUpdate body) {
		Provider provider = serivices.updateProvider(body.thisToProvider());

		provider.setAddress(body.getAddress());

		return successResponse(provider, "success");

	}
}
