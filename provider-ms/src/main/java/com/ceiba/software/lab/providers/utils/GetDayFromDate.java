package com.ceiba.software.lab.providers.utils;

import java.util.Calendar;
import java.util.Date;

public class GetDayFromDate {
	
	public static boolean isSunnday() {
		Date now = new Date();
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(now);
		int day = calendar.get(Calendar.DAY_OF_WEEK);
		return (day == 2);
	}
}
