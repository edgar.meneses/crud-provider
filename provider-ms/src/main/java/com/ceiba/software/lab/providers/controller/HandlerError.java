package com.ceiba.software.lab.providers.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.ceiba.software.lab.providers.exceptions.BeanBadRequestException;
import com.ceiba.software.lab.providers.exceptions.BeanNotAutorizedException;
import com.ceiba.software.lab.providers.model.response.TraceError;

@RestControllerAdvice
@RestController
public class HandlerError {

	@ExceptionHandler(BeanBadRequestException.class)
	public final ResponseEntity<TraceError> handlerBadrequest(BeanBadRequestException error){
		return new ResponseEntity<>(
				new TraceError(
						error.getMessage()
						), 
				HttpStatus.BAD_REQUEST);
	}
	

	@ExceptionHandler(BeanNotAutorizedException.class)
	public final ResponseEntity<TraceError> handlerNotAutorizedExpection(BeanNotAutorizedException error){
		return new ResponseEntity<>(
				new TraceError(
						error.getMessage()
						), 
				HttpStatus.UNAUTHORIZED);
	}

}
