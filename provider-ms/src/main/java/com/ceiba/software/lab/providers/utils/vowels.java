package com.ceiba.software.lab.providers.utils;

public class vowels {

	public static boolean containThreeVowels(String word) {
		int contador = 0;

		for(int x=0;x<word.length();x++) {
			if ((word.charAt(x)=='a') || 
					(word.charAt(x)=='e') ||
					(word.charAt(x)=='i') || 
					(word.charAt(x)=='o') || 
					(word.charAt(x)=='u'))
			{
				contador++;
			}
		}
		return contador > 3;
	}

	public static boolean isPalindrome(String word){
		boolean valor=true;
		int i,ind;
		String auxWord="";

		for (int x=0; x < word.length(); x++) {
			if (word.charAt(x) != ' ')
				auxWord += word.charAt(x);
		}

		word=auxWord;
		ind=word.length();

		for (i= 0 ;i < (word.length()); i++){
			if (word.substring(i, i+1).equals(word.substring(ind - 1, ind)) == false ) {

				valor=false;
				break;
			}
			ind--;
		}
		return valor;
	}
}
