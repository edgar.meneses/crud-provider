package com.ceiba.software.lab.providers.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ceiba.software.lab.providers.model.Provider;

@Repository
public interface ProvidersRepository extends JpaRepository<Provider, Long>{
}
