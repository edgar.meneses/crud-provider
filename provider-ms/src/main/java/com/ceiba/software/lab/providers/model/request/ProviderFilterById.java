package com.ceiba.software.lab.providers.model.request;

import javax.validation.constraints.NotNull;

public class ProviderFilterById {
	
	@NotNull
	private long code;
	
	public ProviderFilterById() {
	}

	public long getCode() {
		return code;
	}

	public void setCode(long code) {
		this.code = code;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (code ^ (code >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProviderFilterById other = (ProviderFilterById) obj;
		if (code != other.code)
			return false;
		return true;
	}

}
