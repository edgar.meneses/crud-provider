package com.ceiba.software.lab.providers.service;

import java.util.List;

import com.ceiba.software.lab.providers.model.Provider;

public interface ProviderService {
	List<Provider> getProviders();
	Provider getProvider(long id);
	Provider createProvider(Provider provider);
	Provider updateProvider(Provider provider);
}
