package com.ceiba.software.lab.providers.utils;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.ceiba.software.lab.providers.model.response.Response;

public class ResponseHandler{

	public static ResponseEntity<Response> successResponse(Object data, String message) {
		Response response = new Response(data, message);
		return new ResponseEntity<Response>(
				response, 
				HttpStatus.OK);
	}
}
