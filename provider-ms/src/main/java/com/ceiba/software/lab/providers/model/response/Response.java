package com.ceiba.software.lab.providers.model.response;

public class Response {
	private Object data;
	private String massage;
	
	public Response() {
	}


	public Response(Object data, String massage) {
		super();
		this.data = data;
		this.massage = massage;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}

	public String getMassage() {
		return massage;
	}

	public void setMassage(String massage) {
		this.massage = massage;
	}

	@Override
	public String toString() {
		return "Response [data=" + data + ", massage=" + massage + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((massage == null) ? 0 : massage.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Response other = (Response) obj;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (massage == null) {
			if (other.massage != null)
				return false;
		} else if (!massage.equals(other.massage))
			return false;
		return true;
	}
}
