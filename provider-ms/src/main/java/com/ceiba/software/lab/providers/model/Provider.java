package com.ceiba.software.lab.providers.model;

import java.io.Serializable; 
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Pattern.Flag;
import javax.validation.constraints.Size;
import static com.ceiba.software.lab.providers.utils.vowels.*;

import org.springframework.lang.NonNull;

import com.ceiba.software.lab.providers.exceptions.BeanBadRequestException;


@Entity
@Table(name = "provider")
public class Provider implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;

	@NotNull
	@Size(min = 5)
	@Column(name = "name")
	private String name;

	@NonNull
	@Column(name = "create_at")
	private Date createAt;

	@NonNull
	@Size(min = 7, max = 7)
	@Pattern(regexp="^[0-9]*$", flags = Flag.UNICODE_CASE)
	@Column(name = "phone")
	private String phone;

	@NonNull
	@Pattern(regexp="^CL[#-a-zA-z0-9 ]*$", flags = Flag.UNICODE_CASE)
	@Column(name = "address")
	private String address;

	public Provider() {
		createAt = new Date();
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		if(isPalindrome(name)) {
			throw new BeanBadRequestException();
		}else {
			this.name = name;
		}
	}

	public Date getCreateAt() {
		return createAt;
	}

	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		if(containThreeVowels(address)) {
			throw new BeanBadRequestException();
		}else {
			this.address = address;
		}
	}

	@Override
	public String toString() {
		return "Provider [id=" + id + ", name=" + name + ", createAt=" + createAt + ", phone=" + phone + ", address="
				+ address + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + ((createAt == null) ? 0 : createAt.hashCode());
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Provider other = (Provider) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (createAt == null) {
			if (other.createAt != null)
				return false;
		} else if (!createAt.equals(other.createAt))
			return false;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		return true;
	}
}
