package com.ceiba.software.lab.providers.model.request;

import javax.persistence.Column;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.constraints.Pattern.Flag;

import org.springframework.lang.NonNull;

import com.ceiba.software.lab.providers.model.Provider;

public class ProviderFilterUpdate {

	private long code;
	private String name;
	private String phone;
	private String address;
	
	public ProviderFilterUpdate() {
	}
	@NotNull
	@Size(min = 5)
	public long getCode() {
		return code;
	}
	@NonNull
	@Column(name = "create_at")
	public void setCode(long code) {
		this.code = code;
	}
	@NonNull
	@Size(min = 7, max = 7)
	public String getName() {
		return name;
	}
	@NonNull
	@Pattern(regexp="^CL[a-zA-z0-9 ]*$", flags = Flag.UNICODE_CASE)
	@Column(name = "address")
	public void setName(String name) {
		this.name = name;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	@Override
	public String toString() {
		return "ProviderFilterUpdate [code=" + code + ", name=" + name + ", phone=" + phone + ", address=" + address
				+ "]";
	}

	public Provider thisToProvider() {
		Provider provider = new Provider();
		provider.setId(this.code);
		provider.setName(this.name);
		provider.setAddress(this.address);
		return provider;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((address == null) ? 0 : address.hashCode());
		result = prime * result + (int) (code ^ (code >>> 32));
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((phone == null) ? 0 : phone.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProviderFilterUpdate other = (ProviderFilterUpdate) obj;
		if (address == null) {
			if (other.address != null)
				return false;
		} else if (!address.equals(other.address))
			return false;
		if (code != other.code)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (phone == null) {
			if (other.phone != null)
				return false;
		} else if (!phone.equals(other.phone))
			return false;
		return true;
	}
}
