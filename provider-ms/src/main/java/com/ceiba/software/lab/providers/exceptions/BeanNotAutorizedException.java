package com.ceiba.software.lab.providers.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BeanNotAutorizedException extends RuntimeException{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public BeanNotAutorizedException() {
		super("Not AUtorized Exception");
	}
}
