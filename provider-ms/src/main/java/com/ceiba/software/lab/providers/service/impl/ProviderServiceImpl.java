package com.ceiba.software.lab.providers.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ceiba.software.lab.providers.model.Provider;
import com.ceiba.software.lab.providers.repository.ProvidersRepository;
import com.ceiba.software.lab.providers.service.ProviderService;

@Service
public class ProviderServiceImpl implements ProviderService{

	@Autowired
	private ProvidersRepository repository;


	public ProviderServiceImpl() {

	}

	@Override
	public List<Provider> getProviders() {
		return repository.findAll();
	}

	@Override
	public Provider getProvider(long id) {
		return repository.findById(id).get();
	}

	@Override
	public Provider createProvider(Provider provider) {
		return repository.save(provider);
	}

	@Override
	public Provider updateProvider(Provider provider) {
		if(repository.existsById(provider.getId())) {
			return repository.save(provider);
		}else {
			return null;
		}
	}
}
